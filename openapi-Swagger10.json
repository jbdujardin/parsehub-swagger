{
  "swaggerVersion": "1.2",
  "basePath": "https://www.parsehub.com/api/v2/projects",
  "resourcePath": "/",
  "apis": [
    {
      "path": "/runs/{RUN_TOKEN}/cancel",
      "operations": [
        {
          "method": "POST",
          "summary": "This cancels a run and changes its status to cancelled. Any data that was extracted so far will be available.",
          "deprecated": false,
          "nickname": "Cancel a job",
          "parameters": [
            {
              "name": "RUN_TOKEN",
              "description": "uid of the run instance",
              "paramType": "path",
              "required": true,
              "allowMultiple": false,
              "defaultValue": "",
              "uniqueItems": false,
              "type": "string"
            },
            {
              "name": "api_key",
              "description": "uid of the user account",
              "paramType": "query",
              "required": false,
              "allowMultiple": false,
              "defaultValue": "",
              "uniqueItems": false,
              "type": "string"
            }
          ],
          "produces": [
            "application/json"
          ],
          "responseMessages": [
            {
              "code": 0,
              "message": "unexpected error"
            }
          ],
          "uniqueItems": false,
          "$ref": "Run"
        }
      ]
    },
    {
      "path": "/projects/{PROJECT_TOKEN}/last_ready_run/data",
      "operations": [
        {
          "method": "GET",
          "summary": "This returns the data for the most recent ready run for a project. You can use this method in order to have a synchronous interface to your project.\n  *Note: The Content-Encoding of this response is always gzip.*\n",
          "deprecated": false,
          "nickname": "This returns the data for the most recent ready run for a project.",
          "parameters": [
            {
              "name": "PROJECT_TOKEN",
              "description": "uid of the project",
              "paramType": "path",
              "required": true,
              "allowMultiple": false,
              "defaultValue": "",
              "uniqueItems": false,
              "type": "string"
            },
            {
              "name": "api_key",
              "description": "uid of the account",
              "paramType": "query",
              "required": false,
              "allowMultiple": false,
              "defaultValue": "",
              "uniqueItems": false,
              "type": "string"
            },
            {
              "name": "format",
              "description": "The format that you would like to get the data in. Possible values csv or json. Defaults to json",
              "paramType": "query",
              "required": false,
              "allowMultiple": false,
              "defaultValue": "",
              "enum": [
                "json",
                "csv"
              ],
              "uniqueItems": false,
              "type": "string"
            }
          ],
          "produces": [
            "application/json"
          ],
          "responseMessages": [
            {
              "code": 0,
              "message": "unexpected error"
            }
          ],
          "uniqueItems": false,
          "$ref": "Project"
        }
      ]
    },
    {
      "path": "/projects/{PROJECT_TOKEN}",
      "operations": [
        {
          "method": "GET",
          "summary": "This will return the project object for a specific project.",
          "deprecated": false,
          "nickname": "This will return the project object for a specific project.",
          "parameters": [
            {
              "name": "PROJECT_TOKEN",
              "description": "",
              "paramType": "path",
              "required": true,
              "allowMultiple": false,
              "defaultValue": "",
              "uniqueItems": false,
              "type": "string"
            },
            {
              "name": "api_key",
              "description": "",
              "paramType": "query",
              "required": true,
              "allowMultiple": false,
              "defaultValue": "",
              "uniqueItems": false,
              "type": "string"
            },
            {
              "name": "include_options",
              "description": "",
              "paramType": "query",
              "required": false,
              "allowMultiple": false,
              "defaultValue": "",
              "uniqueItems": false,
              "type": "string"
            },
            {
              "name": "offset",
              "description": "",
              "paramType": "query",
              "required": false,
              "allowMultiple": false,
              "defaultValue": "",
              "uniqueItems": false,
              "type": "string"
            }
          ],
          "produces": [
            "application/json"
          ],
          "responseMessages": [
            {
              "code": 0,
              "message": "unexpected error"
            }
          ],
          "uniqueItems": false,
          "$ref": "Project"
        }
      ]
    },
    {
      "path": "/projects",
      "operations": [
        {
          "method": "GET",
          "summary": "This gets a list of projects in your account",
          "deprecated": false,
          "nickname": "Lists projects",
          "parameters": [
            {
              "name": "api_key",
              "description": "",
              "paramType": "query",
              "required": true,
              "allowMultiple": false,
              "defaultValue": "",
              "uniqueItems": false,
              "type": "string"
            },
            {
              "name": "include_options",
              "description": "",
              "paramType": "query",
              "required": false,
              "allowMultiple": false,
              "defaultValue": "",
              "uniqueItems": false,
              "type": "string"
            },
            {
              "name": "offset",
              "description": "",
              "paramType": "query",
              "required": false,
              "allowMultiple": false,
              "defaultValue": "",
              "uniqueItems": false,
              "type": "string"
            },
            {
              "name": "limit",
              "description": "",
              "paramType": "query",
              "required": false,
              "allowMultiple": false,
              "defaultValue": "",
              "uniqueItems": false,
              "type": "string"
            }
          ],
          "produces": [
            "application/json"
          ],
          "responseMessages": [
            {
              "code": 0,
              "message": "unexpected error"
            }
          ],
          "uniqueItems": false,
          "$ref": "ProjectList"
        }
      ]
    },
    {
      "path": "/{PROJECT_TOKEN}/run",
      "operations": [
        {
          "method": "POST",
          "summary": "This will start running an instance of the project on the ParseHub cloud. It will create a new run object. This method will return immediately, while the run continues in the background. You can use webhooks or polling to figure out when the data for this run is ready in order to retrieve it.\n",
          "deprecated": false,
          "nickname": "Starts a running instance on the ParseHubCloud",
          "parameters": [
            {
              "name": "PROJECT_TOKEN",
              "description": "UID of the project to run",
              "paramType": "path",
              "required": true,
              "allowMultiple": false,
              "defaultValue": "",
              "uniqueItems": false,
              "type": "string"
            },
            {
              "name": "api_key",
              "description": "API key for your account",
              "paramType": "query",
              "required": true,
              "allowMultiple": false,
              "defaultValue": "",
              "uniqueItems": false,
              "type": "string"
            },
            {
              "name": "start_url",
              "description": "The url to start running on. Defaults to the project’s start_site.",
              "paramType": "query",
              "required": false,
              "allowMultiple": false,
              "defaultValue": "",
              "uniqueItems": false,
              "type": "string"
            },
            {
              "name": "start_template",
              "description": "(Optional)   The template to start running with. Defaults to the projects’s start_template (inside the options_json).",
              "paramType": "query",
              "required": false,
              "allowMultiple": false,
              "defaultValue": "",
              "uniqueItems": false,
              "type": "string"
            },
            {
              "name": "start_value_override",
              "description": "(Optional)   The starting global scope for this run. This can be used to pass parameters to your run. For example, you can pass {\"query\": \"San Francisco\"} to use the query somewhere in your run. Defaults to the project’s start_value.",
              "paramType": "query",
              "required": false,
              "allowMultiple": false,
              "defaultValue": "",
              "uniqueItems": false,
              "type": "string"
            },
            {
              "name": "send_email",
              "description": "(Optional)   If set to anything other than 0, send an email when the run either completes successfully or fails due to an error. Defaults to 0.",
              "paramType": "query",
              "required": false,
              "allowMultiple": false,
              "defaultValue": "0",
              "minimum": "",
              "maximum": "",
              "uniqueItems": false,
              "type": "integer",
              "format": "int32"
            }
          ],
          "produces": [
            "application/json"
          ],
          "responseMessages": [
            {
              "code": 0,
              "message": "unexpected error"
            }
          ],
          "uniqueItems": false,
          "$ref": "Run"
        }
      ]
    },
    {
      "path": "/runs/{RUN_TOKEN}",
      "operations": [
        {
          "method": "GET",
          "summary": "You can call this method repeatedly to poll for when a run is done, though we recommend using a webhook instead. This method is rate-limited. For each run, you may make at most 25 calls during the first 5 minutes after the run started, and at most one call every 3 minutes after that.",
          "deprecated": false,
          "nickname": "Returns the run object for a given run token",
          "parameters": [
            {
              "name": "RUN_TOKEN",
              "description": "",
              "paramType": "path",
              "required": true,
              "allowMultiple": false,
              "defaultValue": "",
              "uniqueItems": false,
              "type": "string"
            },
            {
              "name": "api_key",
              "description": "API key for your account",
              "paramType": "query",
              "required": false,
              "allowMultiple": false,
              "defaultValue": "",
              "uniqueItems": false,
              "type": "string"
            }
          ],
          "produces": [
            "application/json"
          ],
          "responseMessages": [
            {
              "code": 0,
              "message": "unexpected error"
            }
          ],
          "uniqueItems": false,
          "$ref": "Run"
        },
        {
          "method": "DELETE",
          "summary": "This cancels a run if running, and deletes the run and its data.",
          "deprecated": false,
          "nickname": "Deletes a run along with data",
          "parameters": [
            {
              "name": "RUN_TOKEN",
              "description": "",
              "paramType": "path",
              "required": true,
              "allowMultiple": false,
              "defaultValue": "",
              "uniqueItems": false,
              "type": "string"
            },
            {
              "name": "api_key",
              "description": "API key for your account",
              "paramType": "query",
              "required": false,
              "allowMultiple": false,
              "defaultValue": "",
              "uniqueItems": false,
              "type": "string"
            }
          ],
          "produces": [
            "application/json"
          ],
          "responseMessages": [
            {
              "code": 0,
              "message": "unexpected error"
            }
          ],
          "uniqueItems": false,
          "$ref": "Run"
        }
      ]
    },
    {
      "path": "/runs/{RUN_TOKEN}/data",
      "operations": [
        {
          "method": "GET",
          "summary": "This returns the data that was extracted by a run.",
          "deprecated": false,
          "nickname": "Gets Data for a run",
          "parameters": [
            {
              "name": "api_key",
              "description": "API key for your account",
              "paramType": "query",
              "required": true,
              "allowMultiple": false,
              "defaultValue": "",
              "uniqueItems": false,
              "type": "string"
            },
            {
              "name": "RUN_TOKEN",
              "description": "",
              "paramType": "path",
              "required": true,
              "allowMultiple": false,
              "defaultValue": "",
              "uniqueItems": false,
              "type": "string"
            },
            {
              "name": "format",
              "description": "The format that you would like to get the data in. Possible values csv or json. Defaults to json",
              "paramType": "query",
              "required": false,
              "allowMultiple": false,
              "defaultValue": "",
              "enum": [
                "json",
                "csv"
              ],
              "uniqueItems": false,
              "type": "string"
            }
          ],
          "produces": [
            "application/json"
          ],
          "responseMessages": [
            {
              "code": 0,
              "message": "unexpected error"
            }
          ],
          "uniqueItems": false,
          "type": "object"
        }
      ]
    }
  ],
  "models": {
    "Run": {
      "id": "Run",
      "required": [
        "run_token"
      ],
      "properties": {
        "project_token": {
          "description": "A globally unique id representing the project that this run belongs to.",
          "defaultValue": "",
          "uniqueItems": false,
          "type": "string"
        },
        "run_token": {
          "description": "A globally unique id representing this run.",
          "defaultValue": "",
          "uniqueItems": false,
          "type": "string"
        },
        "status": {
          "description": "",
          "defaultValue": "",
          "enum": [
            "initialized",
            "queued",
            "running",
            "cancelled",
            "complete",
            "error"
          ],
          "uniqueItems": false,
          "type": "string"
        },
        "data_ready": {
          "description": "",
          "defaultValue": "",
          "enum": [
            "initialized",
            "queued",
            "running",
            "cancelled",
            "complete",
            "error"
          ],
          "uniqueItems": false,
          "type": "string"
        },
        "start_time": {
          "description": "The time that this run was started at, in UTC +0000.",
          "defaultValue": "",
          "uniqueItems": false,
          "type": "string",
          "format": "date-time"
        },
        "end_time": {
          "description": "The time that this run was stopped. This field will be null if the run is either initialized or running. Time is in UTC +0000.",
          "defaultValue": "",
          "uniqueItems": false,
          "type": "string",
          "format": "date-time"
        },
        "pages": {
          "description": "The number of pages that have been traversed by this run so far.",
          "defaultValue": "",
          "uniqueItems": false,
          "type": "string"
        },
        "md5sum": {
          "description": "The md5sum of the results. This can be used to check if any results data has changed between two runs.",
          "defaultValue": "",
          "uniqueItems": false,
          "type": "string"
        },
        "start_url": {
          "description": "The url that this run was started on.",
          "defaultValue": "",
          "uniqueItems": false,
          "type": "string"
        },
        "start_template": {
          "description": "The template that this run was started with.",
          "defaultValue": "",
          "uniqueItems": false,
          "type": "string"
        },
        "start_value": {
          "description": "The starting value of the global scope for this run.",
          "defaultValue": "",
          "uniqueItems": false,
          "type": "string"
        }
      },
      "subTypes": []
    },
    "Project": {
      "id": "Project",
      "required": [],
      "properties": {
        "token": {
          "description": "A globally unique id representing this project.",
          "defaultValue": "",
          "uniqueItems": false,
          "type": "string"
        },
        "title": {
          "description": "The title give by the user when creating this project.",
          "defaultValue": "",
          "uniqueItems": false,
          "type": "string"
        },
        "templates_json": {
          "description": "The JSON-stringified representation of all the instructions for running this project. This representation is not yet documented, but will eventually allow developers to create plugins for ParseHub.",
          "defaultValue": "",
          "uniqueItems": false,
          "type": "object"
        },
        "main_template": {
          "description": "The name of the template with which ParseHub should start executing the project.",
          "defaultValue": "",
          "uniqueItems": false,
          "type": "string"
        },
        "main_site": {
          "description": "The default URL at which ParseHub should start running the project.",
          "defaultValue": "",
          "uniqueItems": false,
          "type": "string"
        },
        "options_json": {
          "description": "An object containing several advanced options for the project.",
          "defaultValue": "",
          "uniqueItems": false,
          "type": "object"
        },
        "last_run": {
          "description": "The run object of the most recently started run (orderd by start_time) for the project.",
          "defaultValue": "",
          "uniqueItems": false,
          "type": "string",
          "format": "date-time"
        },
        "last_ready_run": {
          "description": "The run object of the most recent ready run (ordered by start_time) for the project. A ready run is one whose data_ready attribute is truthy. The last_run and last_ready_run for a project may be the same.",
          "defaultValue": "",
          "uniqueItems": false,
          "type": "object"
        }
      },
      "subTypes": []
    },
    "ProjectList": {
      "id": "ProjectList",
      "required": [],
      "properties": {
        "projects": {
          "description": "",
          "items": {
            "$ref": "Project"
          },
          "defaultValue": "",
          "uniqueItems": false,
          "type": "array"
        },
        "total_projects": {
          "description": "The total number of projects in your account.",
          "defaultValue": "",
          "minimum": "",
          "maximum": "",
          "uniqueItems": false,
          "type": "integer",
          "format": "int32"
        }
      },
      "subTypes": []
    },
    "Error34": {
      "id": "Error34",
      "required": [
        "code",
        "message"
      ],
      "properties": {
        "code": {
          "description": "",
          "defaultValue": "",
          "minimum": "",
          "maximum": "",
          "uniqueItems": false,
          "type": "integer",
          "format": "int32"
        },
        "message": {
          "description": "",
          "defaultValue": "",
          "uniqueItems": false,
          "type": "string"
        }
      },
      "subTypes": []
    }
  },
  "authorizations": {},
  "info": {
    "title": "ParseHub",
    "description": "OpenAPI definitions for [Parsehub] (https://www.parsehub.com)"
  },
  "consumes": [
    "application/json"
  ],
  "produces": [
    "application/json"
  ]
}